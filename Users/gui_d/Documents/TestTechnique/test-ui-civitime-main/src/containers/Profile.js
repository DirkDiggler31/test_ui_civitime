import React, { Component } from "react";
import man from '../assets/man.svg'
import woman from '../assets/woman.svg'
import color1 from '../assets/color-1.svg'
import color2 from '../assets/color-2.svg'
import color3 from '../assets/color-3.svg'
import color4 from '../assets/color-4.svg'
import color5 from '../assets/color-5.svg'
import color6 from '../assets/color-6.svg'
import color7 from '../assets/color-7.svg'
import stripe from '../assets/color-striped.svg'
import skin from '../assets/skin.svg'
import tshirt from '../assets/t-shirt.svg'
import Button from '../components/Button/Button'


class Profile extends Component {

  render() {
    return (
      <div class="card">
        <div class="card__header">
          <h5 class="card__title">Profil</h5>
        </div>
        <div>
          <p>Lequel de ces deux avatars préférez vous ?</p>
        </div>
        <div class="container__image">
          <img src={man} alt="Person" class="card__image" />
          <img src={woman} alt="Person" class="card__image" />
        </div>
        <div class="line"></div>
        <ul class="skin_color">
          <li><img src={skin}></img></li>
          <li><img className='color' src={color6}></img></li>
          <li><img className='color' src={color4}></img></li>
          <li><img className='color' src={color5}></img></li>
          <li><img className='color' src={color2}></img></li>
        </ul>

        <ul class="skin_color">
          <li><img src={tshirt}></img></li>
          <li><img className='color' src={color3}></img></li>
          <li><img className='color' src={color1}></img></li>
          <li><img className='color' src={color7}></img></li>
          <li><img className='color' src={stripe}></img></li>
        </ul>
        <Button style='btn' title='Valider'></Button>
      </div>
    )
  }
}


export default Profile;