import React from 'react'
import './App.css';
import Profile from './containers/Profile';

const App = () => {
    return <div className="overlay"><Profile /> </div>
}

export default App;
