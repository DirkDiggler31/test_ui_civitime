import React from "react";

const button = (props) => (
    <button class={props.style}>{props.title}</button>
)

export default button